from __future__ import absolute_import
from datetime import datetime

from selenium_apollo import conf
from selenium_apollo.database import ApolloDatabase
from selenium_apollo.event import Event
from pprint import pprint

"""
Adds events to database using configuration from yaml file
"""

events = [
    # Event(name='Wellness', dt=datetime(2017, 9, 30, 15, 0, 0))
]
config = conf.get_config()
database = ApolloDatabase(config['database']['db_engine'], config['database']['tablename'])

with database.session_scope() as ses:
    ses.add_all(events)
    pprint(ses.query(Event).all())
