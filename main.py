from __future__ import absolute_import
import time

import schedule

from selenium_apollo import conf
from selenium_apollo.controller import ApolloController
from selenium_apollo.database import ApolloDatabase
from selenium_apollo.setup_logging import setup_logging


def main():
    cfg = conf.get_config()
    database = ApolloDatabase(cfg['database']['db_engine'], cfg['database']['tablename'])
    with ApolloController(database, cfg) as reg:
        reg.register()


if __name__ == '__main__':
    cfg = conf.get_config()
    setup_logging(cfg['logging']['conf_path'], cfg['logging']['level'])

    schedule.every(cfg['settings']['check_interval']).minutes.do(main)
    while True:
        schedule.run_pending()
        time.sleep(1)
