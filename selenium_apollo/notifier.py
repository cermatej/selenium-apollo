from builtins import object
from pushover import Client


class ApolloNotifier(object):
    """Sends mobile notifications using Pushover"""

    def __init__(self, user_key, api_token):
        self.pushover = Client(user_key=user_key, api_token=api_token)

    def event_registered_notification(self, event):
        msg = 'Event successfully registered {}'.format(event)
        self.pushover.send_message(msg)
