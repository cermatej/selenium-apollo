from __future__ import absolute_import
from __future__ import division
from builtins import next
from builtins import object
from past.utils import old_div
import logging
import time
from datetime import datetime

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from .event import Event
from .notifier import ApolloNotifier


class ApolloController(object):
    """Controls webdriver to register favourite events"""
    LOGIN_URL = 'https://www.supersaas.cz/schedule/login/corinthia/apollo'
    FREE_EVENTS = 'http://www.supersaas.cz/schedule/corinthia/apollo?view=free'

    def __init__(self, database, cfg):
        self.database = database
        self.username = cfg['cred']['username']
        self.fullname = cfg['cred']['fullname']
        self.hours_before = cfg['settings']['hours_before']
        self.wd = webdriver.Remote(
            command_executor='http://selenium-server:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME.copy()
        )
        self.sleep_time = cfg['settings']['sleep_time']
        self.wait = WebDriverWait(self.wd, self.sleep_time)
        self.notifier = ApolloNotifier(cfg['pushover']['user_key'], cfg['pushover']['api_token'])
        self.favourite_events = self.get_favourite_events()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.wd.close()

    def get_favourite_events(self):
        """Queries database for favourite events to register
        :return: Events list
        """
        with self.database.session_scope() as ses:
            events = ses.query(Event).all()
        return events

    def register(self):
        """Registers favourite events if found"""
        if self.favourite_events:
            self.apollo_login()
            self.register_favourite()
        else:
            logging.info('No events to register')

    def open_page(self, url):
        """Opens url and waits specified amount of time"""
        self.wd.get(url)
        time.sleep(self.sleep_time)

    def apollo_login(self):
        """Opens apollo log in page, insert credentials and confirms"""
        logging.info('Logging to apollo website ( username=\'{}\' )'.format(self.username))
        self.open_page(self.LOGIN_URL)
        name_input = self.wait.until(EC.presence_of_element_located((By.NAME, 'name')))
        confirm_elem = self.wait.until(EC.presence_of_element_located((By.NAME, 'commit')))
        self.insert_confirm(name_input, self.username, confirm_elem)

    def get_free_events_page(self):
        self.open_page(self.FREE_EVENTS)

    def register_favourite(self):
        """Cycle through free events and registers favourite"""
        self.get_free_events_page()
        while True:
            to_reg = self.find_event_to_register()
            if to_reg:
                self.register_event(to_reg)
                self.get_free_events_page()
                continue
            try:
                self.next_page()
            except TimeoutException:
                break

    def get_apollo_events(self):
        """Scrapes all events on current page
        :return: Event list
        """
        elements = self.wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'tr.j')))
        return [Event.from_element(element) for element in elements]

    def next_page(self):
        """Clicks at next page button if there is any"""
        next_page = self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, '.pagination > .current + a')))
        next_page.click()
        time.sleep(self.sleep_time)

    def is_oncoming(self, event):
        td = event.dt - datetime.now()
        return float(old_div(td.seconds, 360)) < self.hours_before

    def register_event(self, event):
        """Tries to register event with predefined credentials
        :param event: Event to register
        """
        event.elem.click()
        if not self.event_is_registered():
            self.wd.execute_script('newbooking()')
            name_input = self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input#booking_full_name')))
            confirm_elem = self.wait.until(EC.presence_of_element_located((By.NAME, 'commit')))
            self.insert_confirm(name_input, self.fullname, confirm_elem)
            logging.info('Event successfully registered ( {} )'.format(event))
            self.notifier.event_registered_notification(event)
        else:
            logging.info('Event already registered ( {} )'.format(event))
        self.remove_event(event)

    def remove_event(self, event):
        """Removes event from class list attr and attached database
        :param event: Event instance to remove
        """
        index = self.favourite_events.index(event)
        with self.database.session_scope() as ses:
            ses.delete(self.favourite_events[index])
        del self.favourite_events[index]

    @classmethod
    def insert_confirm(cls, input_elem, text, confirm_elem):
        """Inserts text and confirms by clicking confirm button
        :param input_elem: element to insert text to
        :param confirm_elem: confirm button element
        """
        input_elem.clear()
        input_elem.send_keys(text)
        confirm_elem.click()

    def find_event_to_register(self):
        """
        :return: First instance of event to register if found
        """
        events = self.get_apollo_events()
        return next((event for event in events if event in self.favourite_events and not self.is_oncoming(event)), None)

    def event_is_registered(self):
        try:
            self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div#bbox_content>table')))
        except TimeoutException:
            return False
        return True
