import logging.config
import os

import yaml


def setup_logging(default_path, default_level):
    """Sets up logging for application
    :param default_path: Relative path to yaml configuration file
    :param default_level: Default logging level
    """
    path = default_path
    if os.path.exists(path):
        with open(path, 'rt') as f:
            cfg = yaml.safe_load(f.read())
        logging.config.dictConfig(cfg)
    else:
        logging.basicConfig(level=default_level)
