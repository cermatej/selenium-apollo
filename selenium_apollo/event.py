from datetime import datetime

from parse import *
from sqlalchemy import Column, String, DateTime, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Event(Base):
    """Represents SQL alchemy ORM object"""
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    dt = Column(DateTime)

    def __init__(self, name, dt, elem=None):
        """
        :param name: eq. 'Fitness','Wellness' etc.
        :param dt: datetime instance defining when event starts
        :param elem: DOM element
        """
        self.name = name
        self.dt = dt
        self.elem = elem

    def __eq__(self, other):
        return self.dt == other.dt and self.name == other.name

    def __str__(self):
        return 'Event {} at {}'.format(self.name, self.dt)

    def __repr__(self):
        return 'Event ( name="{}", datetime="{}" )'.format(self.name, self.dt)

    def __cmp__(self, other):
        return object.__cmp__(self, other)

    def __hash__(self):
        return object.__hash__(self)

    @classmethod
    def from_element(cls, elem):
        """Create instance from DOM element
        :param elem: DOM element
        :return: Event instance
        """
        name = cls.get_event_name(elem)
        dt = cls.get_event_dt(elem)
        return Event(name=name, dt=dt, elem=elem)

    @classmethod
    def get_event_name(cls, elem):
        return elem.find_element_by_css_selector('.lt + td').text

    @classmethod
    def get_event_dt(cls, elem):
        date_text = elem.find_element_by_css_selector('.hv.rt').text
        return cls.dt_convert(date_text)

    @classmethod
    def dt_convert(cls, event_text):
        res = parse('{}.{} {}:{}', event_text[3:])
        return datetime(datetime.today().year, int(res[1]), int(res[0]), int(res[2]), int(res[3]))
