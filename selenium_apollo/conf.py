import os

import yaml


def get_config(file_path='conf/selenium-apollo.yaml'):
    """
    :param file_path: Relative path to yaml configuration file
    :return: Returns config from yaml file for the whole application
    """
    if os.path.exists(file_path):
        with open(file_path, 'r') as stream:
            return yaml.safe_load(stream)
    raise Exception('No apollo configuration file found')
