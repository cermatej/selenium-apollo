from __future__ import absolute_import
from builtins import object
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class ApolloDatabase(object):
    """Maintains database for application"""

    def __init__(self, db_engine, tablename):
        """
        :param db_engine: Consists of database engine and relative path to database
        :param tablename: Database table name
        """
        self.engine = create_engine(db_engine)
        self.Session = sessionmaker(expire_on_commit=False)
        self.Session.configure(bind=self.engine)
        if not self.engine.dialect.has_table(self.engine, tablename):
            from .event import Base
            Base.metadata.create_all(self.engine)

    @contextmanager
    def session_scope(self):
        """Returns context manager with database session for convenient use"""
        session = self.Session()
        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
